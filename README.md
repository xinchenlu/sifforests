# README #

### What is this repository for? ###

* This is a supplementary source for the manuscript:
* Seasonal patterns of canopy photosynthesis captured by remotely sensed sun-induced fluorescence and vegetation indexes in mid-to-high latitude forests: a cross-platform comparison.
* You Can have access to the data and codes of this study.
* We would like to appreciate your interest in our study.
* In any cases a problem raises, please contact Xinchen Lu (stevenlvrs@foxmail.com).
* Xinchen is a self-motivated student who is looking for a PhD position. If you have any information regarding PhD opportunities in the field of ecology and remote sensing, Xinchen would like to appreciate you if you can inform him.

### How do I get set up? ###

* All the codes of this projects are generated using Matlab
* Here is the link to the platform: https://ww2.mathworks.cn/
* We have downloaded some data from several data portals including Fluxnet and MODIS, if you want to use some of those data in a way that may lead to publications, please ensure you enforce the regarding data policies.

### Contribution guidelines ###

* If you find our studies interesting, in addition to cite this manuscript, I would suggest you to look at publications of Xinchen Lu via ResearchGate:



### Who do I talk to? ###

* Xinchen Lu (stevenlvrs@foxmail.com)